<?php 

trait debugTrait {
    
    private $DEBUG = true; // change $DEBUG ONLY here to disable debug ouput
    
    public function pr($var) 
    {
        if ($this->DEBUG) 
        { 
            echo '<pre>'; 
            print_r($var); 
            echo '</pre>'; 
        }
    }
    
    public function isDebugMode()
    {
        return $this->DEBUG; 
    }
}
