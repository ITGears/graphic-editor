<?php

class ShapePool {
    
    use debugTrait;
    
    // only publicly available properties can by accessed via DI
    // as it's not inherited in the 'normal' way
    public $shape_pool;
    
    
    /**
     * 
     * @param $shape_params array of elements ['type' => <class name>, 'params' => <custom params for this type of shape>]
     * @param $debug int 1 = show debug info
     */ 
    function __construct($shape_params)
    {
        foreach($shape_params as $sp)
        {
            $shape = $sp['type']; // shape name
            require_once 'shapes/'.$shape.'.php'; // load class from file
            if (class_exists($shape))
            {
                $this->shape_pool[] = new $shape($sp['params'], $debug);
            }
        }
        
        if ($this->isDebugMode())
        {
            echo "Shape pool:";
            $this->pr($this->shape_pool);
        }
    } // __construct
    
    
    function __destruct()
    {
        unset($this->shape_pool);
        if ($this->isDebugMode())
        {
            echo "<p>ShapePool.cleaned up...</p>";
        }     
    }
    
}