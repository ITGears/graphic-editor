<?php 
require_once("DebugTrait.php"); 

require_once("Shape.php");
require_once("ShapeInterface.php");
require_once("ShapePool.php");
require_once("GraphicEditor.php");


$shape_params = [
    ['type' => 'Circle', 'params' => ['color' => '#0000ff', 'x' => 10, 'y' => 10, 'radius' => 10]],
    ['type' => 'Circle', 'params' => ['color' => '#ff0000', 'x' => 40, 'y' => 40, 'radius' => 20, 'filled' => true, 'filled_color' => '#ffffff']],
    ['type' => 'Rectangle', 'params' => ['color' => '#00ff00', 'x1' => 80, 'y1' => 40, 'x2' => 120, 'y2' => 50]],
];

// crete shape pool to add it as a param into out editor
$shape_pool = new ShapePool($shape_params);

// GraphicEditor does not know about injected shapes
$editor = new GraphicEditor($shape_pool); 

$editor->draw();
 
