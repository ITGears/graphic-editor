<?php 

class Rectangle extends Shape implements ShapeInterface {

    use debugTrait;
     
    function __construct($params)
    {
        $this->type = 'rectangle';
        $this->params = [
            'x1' => (@$params['x1']) ?: 100, 
            'y1' => (@$params['y1']) ?: 100, 
            'x2' => (@$params['x2']) ?: 100, 
            'y2' => (@$params['y2']) ?: 100, 
            'color' => (@$params['color']) ?: '#000000',
        ];
    }
    
    
    /**
      * Draw primitive
      *
      */
    public function draw($screen)
    {
        parent::draw($screen);
        if ($this->isDebugMode()) return; // do not draw in debug mode
        
        $rgb = $this->hex2rgb($this->params['color']);
        imagerectangle($screen,
            $this->params['x1'],
            $this->params['y1'],
            $this->params['x2'],
            $this->params['y2'],
            imagecolorallocate($screen, $rgb[0], $rgb[1], $rgb[2]));
    }
    
}

