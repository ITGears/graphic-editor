<?php 

class Circle extends Shape implements ShapeInterface {
     
    use debugTrait;
    
    function __construct($params)
    {
        $this->type = 'circle';
        $this->params = [
            'x_center' => (@$params['x']) ?: 100, 
            'y_center' => (@$params['y']) ?: 100,
            'radius' => (@$params['radius']) ?: 100,
            'color' => (@$params['color']) ?: '#000000',
            'filled' => (@$params['filled']) ?: false,
            'filled_color' => (@$params['filled_color']) ?: '#ffffff',
        ];
    }
    
    
    /**
      * Draw primitive
      *
      */
    public function draw($screen)
    {
        parent::draw($screen);
        if ($this->isDebugMode()) return; // do not draw in debug mode
        
        // output filled circle
        if ($this->params['filled']) 
        {
            $rgb1 = $this->hex2rgb($this->params['filled_color']);
            imagefilledellipse($screen, 
                $this->params['x_center'], 
                $this->params['y_center'], 
                $this->params['radius'], 
                $this->params['radius'], 
                imagecolorallocate($screen, $rgb1[0], $rgb1[1], $rgb1[2]));                    
        } 
        
        // output circle without filling
        $rgb = $this->hex2rgb($this->params['color']);
        imageellipse($screen, 
            $this->params['x_center'], 
            $this->params['y_center'], 
            $this->params['radius'], 
            $this->params['radius'], 
            imagecolorallocate($screen, $rgb[0], $rgb[1], $rgb[2]));
     }
     
}
