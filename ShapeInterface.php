<?php 

interface ShapeInterface {
    
    // implementation is different in each inherited class (shape type) and is a mandatory
    public function draw($screen);
    
}
