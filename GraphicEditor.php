<?php 

class GraphicEditor {
    
    use debugTrait;
    
    private $primitives = []; // array of available primitives
    
    const SCREEN_WIDTH = 300;
    const SCREEN_HEIGHT = 300;
    private $screen; // area SCREEN_WIDTH x SCREEN_HEIGHT where to ouput $primitives
    
    
    function __construct($shapes_pool) 
    {
        if ($this->isDebugMode()) {
            echo "Initialisation...";
        }
        $this->primitives = $shapes_pool;
    }
    
    /**
      * Draw primitives
      *
      */
    public function draw()
    {
        $this->screen = NULL;
        
        //
        // Debug mode: output only data structures and actions
        //
        if ($this->isDebugMode())
        {
            foreach($this->primitives->shape_pool as $shape)
            {
                $shape->draw($this->screen);
            }
            
        } else {
            //
            // Live mode: draw primitives
            //
            $this->screen = imagecreatetruecolor(self::SCREEN_WIDTH, self::SCREEN_HEIGHT);
            foreach($this->primitives->shape_pool as $shape)
            {
                $shape->draw($this->screen);
            }
            header("Content-type: image/png");
            imagepng($this->screen);
        }
    }
    
    
    function __destruct()
    {
        unset($this->primitives);
        if ($this->isDebugMode())
        {
            echo "<p>GraphicEditor.cleaned up...</p>";
        }     
    }
    
}
