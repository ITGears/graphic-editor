<?php 

abstract class Shape {
    
    // name of shape: Circle, Rectangle etc.
    protected $type; 
    
    // passed params: color, coordinaets etc.
    protected $params; 
    
    
    /**
      * What is the type of this shape
      *
      * @return string shape name
      */
    public function getType()
    {
        return $this->type;
    }
    
    
    /**
      * Draw primitive
      *
      */
    public function draw($screen)
    {
        if ($this->isDebugMode())
        {
            echo "<p>~ drawing {$this->type}...</p>";
        }        
    }
    
    /**
      * Convert color from #xxxxxx format into array(red,green,blue) format
      * @param $hex string in hex format. For example: #ff00aa
      * @return array (0..255, 0..255, 0..255)
      **/ 
    public function hex2rgb($hex) 
    {    
        $hex = str_replace("#", "", $hex);
        $r = (strlen($hex) == 3) ? hexdec(substr($hex,0,1).substr($hex,0,1)) : hexdec(substr($hex,0,2));
        $g = (strlen($hex) == 3) ? hexdec(substr($hex,1,1).substr($hex,1,1)) : hexdec(substr($hex,2,2));
        $b = (strlen($hex) == 3) ? hexdec(substr($hex,2,1).substr($hex,2,1)) : hexdec(substr($hex,4,2));
        return array($r, $g, $b); // returns an array with the rgb values
    }
    
}