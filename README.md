# Graphic Editor #
To add new shape it's needed two steps:

* create a new class under `/shapes` folder with the structure similar to Circle, i.e inherit `Shape` class and implements `ShapeInterface`

* add a new element into `$shape_params` variable with the `type` equal to the newly created shape class name and corresponding `params` after 


#### Debugging

* debugTrait.$DEBUG set to true/false